package com.capgemini.greenhouse.lib.sensors;

import com.capgemini.greenhouse.lib.TopicListener;

import java.beans.PropertyChangeEvent;
import java.util.logging.Logger;

public class MoistureSensorObserver extends SensorObserver {

    private Logger logger = Logger.getLogger(MoistureSensorObserver.class.getName());

    private Double moisture = 0.00;
    public MoistureSensorObserver(TopicListener listener) {
        super(listener);
    }

    private void setTemperatureFromPayload(String payload) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        setTemperatureFromPayload(evt.getNewValue().toString());
        super.propertyChange(evt);
    }
}
