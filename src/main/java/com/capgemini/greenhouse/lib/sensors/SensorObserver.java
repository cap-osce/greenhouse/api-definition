package com.capgemini.greenhouse.lib.sensors;

import com.capgemini.greenhouse.lib.TopicListener;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Logger;

public class SensorObserver implements PropertyChangeListener {
    private Logger logger = Logger.getLogger(SensorObserver.class.getName());

    private String topic;

    public SensorObserver(TopicListener listener) {
        topic = listener.getTopic();
        listener.addChangeListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        //NOOP
    }
}
