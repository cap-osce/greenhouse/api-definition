package com.capgemini.greenhouse.lib.sensors;

import com.capgemini.greenhouse.lib.TopicListener;

import java.beans.PropertyChangeEvent;
import java.util.logging.Logger;

public class LightSensorObserver extends SensorObserver {

    private Logger logger = Logger.getLogger(LightSensorObserver.class.getName());

    private Double light = 0.00;
    public LightSensorObserver(TopicListener listener) {
        super(listener);
    }

    private void setTemperatureFromPayload(String payload) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        setTemperatureFromPayload(evt.getNewValue().toString());
        super.propertyChange(evt);
    }
}
