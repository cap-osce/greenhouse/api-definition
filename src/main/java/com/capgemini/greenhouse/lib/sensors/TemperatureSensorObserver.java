package com.capgemini.greenhouse.lib.sensors;

import com.capgemini.greenhouse.lib.TopicListener;
import com.capgemini.osce.greenhouse.api.sensor.SensorSchema;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyChangeEvent;
import java.io.IOException;

public class TemperatureSensorObserver extends SensorObserver {

    private static final Logger LOGGER = LoggerFactory.getLogger(TemperatureSensorObserver.class);

    private ObjectMapper mapper = new ObjectMapper();

    private Double temperature = 0.00;
    public TemperatureSensorObserver(TopicListener listener) {
        super(listener);
    }

    private void setTemperatureFromPayload(String payload) {
        try {
            SensorSchema sensor = mapper.readerFor(SensorSchema.class).readValue(payload);
            temperature = Double.parseDouble(sensor.getData().getData());
            String sensorId = sensor.getSensor().getName();
            String sensorType = sensor.getSensor().getType();
            LOGGER.info("Sensor : {} of type: {} has received temperature reading of {}",
                    sensorId,
                    sensorType,
                    temperature);
        } catch (IOException e) {
            LOGGER.error("Failed to read sensor data", e);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        setTemperatureFromPayload(evt.getNewValue().toString());
        super.propertyChange(evt);
    }
}
