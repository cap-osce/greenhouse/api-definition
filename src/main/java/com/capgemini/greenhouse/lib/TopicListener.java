package com.capgemini.greenhouse.lib;

import com.amazonaws.services.iot.client.AWSIotMessage;
import com.amazonaws.services.iot.client.AWSIotQos;
import com.amazonaws.services.iot.client.AWSIotTopic;
import com.capgemini.greenhouse.services.MQTTService;
import org.springframework.beans.factory.annotation.Autowired;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

public class TopicListener extends AWSIotTopic {

    private List<PropertyChangeListener> listeners = new ArrayList<>();

    @Autowired
    private MQTTService service;

    public TopicListener(String topic) {
        super(topic, AWSIotQos.QOS1);
    }

    @Override
    public void onMessage(AWSIotMessage message) {
        notifyListener(message.getStringPayload());
    }

    public void addChangeListener(PropertyChangeListener listener) {
        listeners.add(listener);
    }

    private void notifyListener(String  newValue) {
        for(PropertyChangeListener listener: listeners) {
            listener.propertyChange(new PropertyChangeEvent(this, "payload", "", newValue));
        }
    }



}
