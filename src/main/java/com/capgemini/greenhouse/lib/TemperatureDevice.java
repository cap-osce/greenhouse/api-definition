package com.capgemini.greenhouse.lib;

import java.util.logging.Logger;

public class TemperatureDevice extends ConnectedShadow {

    private Logger logger = Logger.getLogger(TemperatureDevice.class.getName());

    public TemperatureDevice(String thingName) {
        super(thingName);
    }

    @Override
    public void onShadowUpdate(String jsonState) {
        super.onShadowUpdate(jsonState);
    }
}
