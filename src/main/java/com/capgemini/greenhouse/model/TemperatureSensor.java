package com.capgemini.greenhouse.model;

public class TemperatureSensor {
    private Sensor sensor;
    private Double value;

    public TemperatureSensor(Sensor sensor, Double value) {
        this.sensor = sensor;
        this.value = value;
    }
}
