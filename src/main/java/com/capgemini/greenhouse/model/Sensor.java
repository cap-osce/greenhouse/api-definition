package com.capgemini.greenhouse.model;

import lombok.Data;

@Data
public class Sensor {
    private String name;
    private String type;

    public Sensor(String name, String type) {
        this.name = name;
        this.type = type;
    }
}
