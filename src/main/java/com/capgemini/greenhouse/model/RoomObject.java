package com.capgemini.greenhouse.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

/**
 * room object
 */
@ApiModel(description = "room object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-03-04T11:36:07.231Z[Europe/London]")
public class RoomObject {
    @JsonProperty("temperature")
    private BigDecimal temperature = null;

    @JsonProperty("humidity")
    private String humidity = null;

    @JsonProperty("light")
    private String light = null;

    public RoomObject temperature(BigDecimal temperature) {
        this.temperature = temperature;
        return this;
    }

    /**
     * Updated temperature of the room
     *
     * @return temperature
     **/
    @ApiModelProperty(value = "Updated temperature of the room")

    @Valid
    public BigDecimal getTemperature() {
        return temperature;
    }

    public void setTemperature(BigDecimal temperature) {
        this.temperature = temperature;
    }

    public RoomObject humidity(String humidity) {
        this.humidity = humidity;
        return this;
    }

    /**
     * Updated humidity of the room
     *
     * @return humidity
     **/
    @ApiModelProperty(value = "Updated humidity of the room")

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public RoomObject light(String light) {
        this.light = light;
        return this;
    }

    /**
     * Updated light of the room
     *
     * @return light
     **/
    @ApiModelProperty(value = "Updated light of the room")

    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RoomObject roomObject = (RoomObject) o;
        return Objects.equals(this.temperature, roomObject.temperature) &&
                Objects.equals(this.humidity, roomObject.humidity) &&
                Objects.equals(this.light, roomObject.light);
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperature, humidity, light);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class RoomObject {\n");

        sb.append("    temperature: ").append(toIndentedString(temperature)).append("\n");
        sb.append("    humidity: ").append(toIndentedString(humidity)).append("\n");
        sb.append("    light: ").append(toIndentedString(light)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
