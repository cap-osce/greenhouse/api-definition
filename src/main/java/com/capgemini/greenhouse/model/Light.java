package com.capgemini.greenhouse.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

/**
 * Light
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-03-04T11:36:07.231Z[Europe/London]")
public class Light {
    @JsonProperty("light")
    private BigDecimal light = null;

    @JsonProperty("unit")
    private String unit = null;

    public Light light(BigDecimal light) {
        this.light = light;
        return this;
    }

    /**
     * Get light
     *
     * @return light
     **/
    @ApiModelProperty(example = "1000.0", value = "")

    @Valid
    public BigDecimal getLight() {
        return light;
    }

    public void setLight(BigDecimal light) {
        this.light = light;
    }

    public Light unit(String unit) {
        this.unit = unit;
        return this;
    }

    /**
     * Get unit
     *
     * @return unit
     **/
    @ApiModelProperty(example = "lumens", value = "")

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Light light = (Light) o;
        return Objects.equals(this.light, light.light) &&
                Objects.equals(this.unit, light.unit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(light, unit);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Light {\n");

        sb.append("    light: ").append(toIndentedString(light)).append("\n");
        sb.append("    unit: ").append(toIndentedString(unit)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
