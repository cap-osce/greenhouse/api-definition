package com.capgemini.greenhouse;

import com.amazonaws.services.iot.client.AWSIotException;
import com.capgemini.greenhouse.services.MQTTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PreDestroy;


@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = {"com.capgemini.greenhouse.swagger",
        "com.capgemini.greenhouse.api",
        "com.capgemini.greenhouse.swagger.configuration",
        "com.capgemini.greenhouse.services",
        "com.capgemini.greenhouse.configuration"
        })
public class IntelligentGreenhouseApplication implements CommandLineRunner {

    @Autowired
    private MQTTService service;

    @Value("#{'${mqtt.topics}'.split(',')}")
    private String[] topics;




    @Override
    public void run(String... arg0) throws Exception {
        if (arg0.length > 0 && arg0[0].equals("exitcode")) {
            throw new IntelligentGreenhouseApplication.ExitException();
        }
        for(String topic: topics) {
            service.addTopic(topic);
        }
        service.connect();
    }

    public static void main(String[] args) throws Exception {
        new SpringApplication(IntelligentGreenhouseApplication.class).run(args);
    }

    @PreDestroy
    public void exitHook() throws AWSIotException {
        service.exit();
    }

    class ExitException extends RuntimeException implements ExitCodeGenerator {
        private static final long serialVersionUID = 1L;

        @Override
        public int getExitCode() {
            return 10;
        }

    }
}
