package com.capgemini.greenhouse.configuration;

import com.capgemini.greenhouse.lib.certificates.KeyStorePasswordPair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!test")
public class GreenhouseConfiguration {

    @Value("${mqtt.certificateFilePath}")
    private String certificateFile;
    @Value("${mqtt.keyFilePath}")
    private String keyFile;

    @Bean
    public KeyStorePasswordPair getKeyStorePair(){
        return KeyStorePasswordPair.getKeyStorePasswordPair(certificateFile, keyFile);
    }
}
