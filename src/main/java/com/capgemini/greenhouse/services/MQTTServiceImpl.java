package com.capgemini.greenhouse.services;


import com.amazonaws.services.iot.client.AWSIotConnectionStatus;
import com.amazonaws.services.iot.client.AWSIotException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
@Profile("!test")
public class MQTTServiceImpl implements MQTTService {

    private Logger logger = Logger.getLogger(MQTTServiceImpl.class.getName());


    @Autowired
    private GreenhouseClient client;

    private AWSIotConnectionStatus connectionStatus;

    @Override
    public void connect() {
        client.intiiateConnectionAndSubscribe();
    }

    @Override
    public void disconnect() throws AWSIotException {
        client.disconnect();
    }

    @Override
    public void addTopic(String topic) throws AWSIotException {
        client.addTopic(topic);
    }
    @Override
    public void attachShadow(String topic) throws AWSIotException {
            client.attachShadow(topic);
        }

    @Override
    public void exit() throws AWSIotException {
        logger.info("Client is exiting");
        client.exit();
    }

}
