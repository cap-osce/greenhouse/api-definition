package com.capgemini.greenhouse.services;

import com.amazonaws.services.iot.client.AWSIotException;

public interface MQTTService {
    void connect();

    void disconnect() throws AWSIotException;

    void addTopic(String topic) throws AWSIotException;

    void attachShadow(String topic) throws AWSIotException;

    void exit() throws AWSIotException;
}
