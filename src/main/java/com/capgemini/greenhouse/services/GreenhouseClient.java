package com.capgemini.greenhouse.services;

import com.amazonaws.services.iot.client.*;
import com.capgemini.greenhouse.lib.ConnectedShadow;
import com.capgemini.greenhouse.lib.sensors.*;
import com.capgemini.greenhouse.lib.TopicListener;
import com.capgemini.greenhouse.lib.certificates.KeyStorePasswordPair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Component
@Profile("!test")
public class GreenhouseClient extends AWSIotMqttClient {
    private Logger logger = Logger.getLogger(GreenhouseClient.class.getName());
    private AWSIotConnectionStatus status = AWSIotConnectionStatus.CONNECTED;



    private Map<String, TopicListener> topics = new HashMap<>();
    private Map<String, ConnectedShadow> shadows = new HashMap<>();
    private Map<String, SensorObserver> sensors = new HashMap<>();

    public GreenhouseClient(KeyStorePasswordPair pair,
                            @Value("${mqtt.endpoint}")String mqttEndPoint,
                            @Value("${mqtt.clientId}") String mqqClientId) {
        super(mqttEndPoint, mqqClientId, pair.keyStore, pair.keyPassword);
        setKeepAliveInterval(2000);
    }

    public void intiiateConnectionAndSubscribe() {
        try {
            connect();
            } catch (AWSIotException e) {
            logger.warning("Connection failure when trying to initiate connection to MQTT");
        }
    }

    public void addTopic(String topic) throws AWSIotException {
        if (topics.get(topic) != null){
            return;
        }
        TopicListener listener = new TopicListener(topic);
        topics.put(topic, listener);
        SensorObserver sensor = null;
        String[] split = topic.split("/");

        switch (split[1].toLowerCase()){
            case "temperature":
                logger.info("Adding new temperature sensor");
                sensor = new TemperatureSensorObserver(listener);
                break;
            case "light":
                logger.info("Adding new light sensor");
                sensor = new LightSensorObserver(listener);
                break;
            case "moisture":
                logger.info("Adding new moisture sensor");
                sensor = new MoistureSensorObserver(listener);
                break;
            case "humidity":
                logger.info("Adding new humidity sensor");
                sensor = new HumiditySensorObserver(listener);
                break;
        }
        sensors.put(topic, sensor);
        subscribe(listener);
    }

    public void removeTopic(String topic) {
        topics.remove(topic);
        sensors.remove(topic);
    }

    public void attachShadow(String shadowName) throws AWSIotException{
        if( shadows.get(shadowName) != null) {
            return;
        }
        ConnectedShadow shadow = new ConnectedShadow(shadowName);
        attach(shadow);
        shadows.put(shadowName, shadow);
    }

    public void detachShadow(String shadowName) throws AWSIotException {
        ConnectedShadow shadow = shadows.get(shadowName);
        if(shadow != null){
            shadows.remove(shadowName);
            detach(shadow);
        }
    }

    @Override
    public void onConnectionSuccess() {
        for (Map.Entry<String, TopicListener> entry :topics.entrySet()) {
            try {
                subscribe(entry.getValue());
            } catch (AWSIotException e) {
                logger.severe("Unable to subscribe to topic");
            }
        }
        AWSIotConnectionStatus new_status = getConnectionStatus();
        if (!new_status.equals(status)) {
            status = new_status;
        }
        super.onConnectionSuccess();
    }

    @Override
    public void onConnectionClosed() {
        AWSIotConnectionStatus new_status = getConnectionStatus();
        if (!new_status.equals(status)) {
            status = new_status;
        }
        super.onConnectionClosed();
    }

    @Override
    public void onConnectionFailure() {
        AWSIotConnectionStatus new_status = getConnectionStatus();
        if (!new_status.equals(status)) {
            status = new_status;
        }
        super.onConnectionFailure();
    }

    public void exit() throws AWSIotException {
        sensors.clear();
        topics.clear();
        disconnect();
    }
}
