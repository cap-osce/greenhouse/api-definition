package com.capgemini.greenhouse.api;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-03-04T11:36:07.231Z[Europe/London]")
class ApiException extends Exception {
    private final int code;

    ApiException(int code, String msg) {
        super(msg);
        this.code = code;
    }
}
