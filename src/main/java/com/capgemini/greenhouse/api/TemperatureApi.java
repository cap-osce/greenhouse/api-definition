package com.capgemini.greenhouse.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

/**
 * NOTE: This class is auto generated by the swagger code generator program (3.0.5).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-03-04T11:36:07.231Z[Europe/London]")
@Api(value = "temperature", description = "the temperature API")
public interface TemperatureApi {

    @ApiOperation(value = "Sets the required temperature for the room", nickname = "updateTemperature", notes = "", tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 405, message = "Invalid input"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 503, message = "Service unavailable")})
    @RequestMapping(value = "/temperature",
            method = RequestMethod.POST)
    ResponseEntity<Void> updateTemperature(@ApiParam(value = "The temperature to set in the room, in celcius.") @Valid @RequestParam(value = "temp", required = false) Integer temp);

}
