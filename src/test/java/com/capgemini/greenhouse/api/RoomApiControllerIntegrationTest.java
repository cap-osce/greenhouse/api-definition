package com.capgemini.greenhouse.api;

import com.capgemini.greenhouse.model.RoomObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class RoomApiControllerIntegrationTest {

    @Autowired
    private RoomApi api;

    @Test
    public void roomHumidityTest() {
        ResponseEntity<Object> responseEntity = api.roomHumidity();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void roomLightTest() {
        ResponseEntity<Object> responseEntity = api.roomLight();
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void roomTemperatureTest() {
        ResponseEntity<Object> responseEntity = api.roomTemperature();
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void updateRoomTest() {
        RoomObject body = new RoomObject();
        ResponseEntity<Void> responseEntity = api.updateRoom(body);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
