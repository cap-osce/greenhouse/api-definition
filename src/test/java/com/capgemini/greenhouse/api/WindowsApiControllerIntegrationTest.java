package com.capgemini.greenhouse.api;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class WindowsApiControllerIntegrationTest {

    @Autowired
    private WindowsApi api;

    @Test
    public void setWindowStateTest() {
        Integer id = 56;
        String state = "state_example";
        ResponseEntity<Void> responseEntity = api.setWindowState(id, state);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
