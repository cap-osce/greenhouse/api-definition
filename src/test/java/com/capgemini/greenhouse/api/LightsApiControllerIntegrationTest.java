package com.capgemini.greenhouse.api;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class LightsApiControllerIntegrationTest {

    @Autowired
    private LightsApi api;

    @Test
    public void setLightLevelTest() {
        Integer id = 56;
        Integer level = 56;
        ResponseEntity<Void> responseEntity = api.setLightLevel(id, level);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
