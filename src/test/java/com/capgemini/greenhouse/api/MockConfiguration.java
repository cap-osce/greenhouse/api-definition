package com.capgemini.greenhouse.api;

import com.capgemini.greenhouse.services.MQTTService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MockConfiguration {

    @Bean
    public MQTTService mqttService() {
        return Mockito.mock(MQTTService.class);
    }

}
